import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";
import Button from "react-bootstrap/Button";
interface columnObject {
  dataField: string;
  text: string;
}
interface iAppProps {}
interface iAppState {
  columns: columnObject[];
  rows: any;
}
class App extends React.Component<iAppProps, iAppState> {
  constructor(props: Readonly<iAppProps>) {
    super(props);
    this.state = {
      rows: [
        {
          id: 1,
          A: 3,
          B: 1
        },
        {
          id: 2,
          A: 4,
          B: 5
        },
        {
          id: 3,
          A: 2,
          B: 4
        }
      ],
      columns: [
        {
          dataField: "id",
          text: ""
        },
        {
          dataField: "A",
          text: "A"
        },
        {
          dataField: "B",
          text: "B"
        }
      ]
    };
    this.onAddRow = this.onAddRow.bind(this);
    this.onAddColumn = this.onAddColumn.bind(this);
    this.beforeSaveCell = this.beforeSaveCell.bind(this);
  }
  onAddRow = () => {
    const columns = this.state.columns;
    const rows = this.state.rows;
    var newCol = columns.map(p => ({ ...p }));
    var newRow = rows.map((p: any) => ({ ...p }));
    var newItem: any = {
      id: newRow.length + 1,
      A: 2,
      B: 4
    };
    for (var i = 0; i <= newCol.length - 1; i++) {
      const value = String.fromCharCode(65 + i) as any;
      newItem[value] = 0;
    }
    newRow.push(newItem);

    this.setState({ columns: newCol, rows: newRow });
  };
  onAddColumn = () => {
    const columns = this.state.columns;
    const rows = this.state.rows;
    var newCol = columns.map(p => ({ ...p }));
    var newRow = rows.map((p: any) => ({ ...p }));
    var newItem = String.fromCharCode(64 + newCol.length);
    console.log(newItem);

    for (var i = 0; i < newRow.length; i++) {
      newRow[i][String.fromCharCode(64 + newCol.length)] = 0;
    }
    newCol.push({ dataField: newItem, text: newItem });
    this.setState({ columns: newCol, rows: newRow });
  };
  // beforeSaveCell(row, cellName, cellValue) {
  beforeSaveCell(oldValue: any, newValue: any, row: any, column: any) {
    const rows = this.state.rows;
    var newRow = rows.map((p: any) => ({ ...p }));
    var firstNum = 0,
      secondNum = 0,
      result = 0;
    if (newValue[0] === "=") {
      for (var i = 0; i < 26; i++) {
        if (newValue[1] === String.fromCharCode(65 + i)) {
          for (var j = 1; j < 10; j++) {
            if (parseInt(newValue[2]) === j) {
              firstNum = newRow[j - 1][newValue[1]];
            }
          }
        }
      }
      for (i = 0; i < 26; i++) {
        if (newValue[4] === String.fromCharCode(65 + i)) {
          for (j = 1; j < 10; j++) {
            if (parseInt(newValue[5]) === j) {
              secondNum = newRow[j - 1][newValue[4]];
            }
          }
        }
      }
      if (newValue[3] === "*") {
        result = firstNum * secondNum;
      } else if (newValue[3] === "/") {
        result = firstNum / secondNum;
      } else if (newValue[3] === "+") {
        result = firstNum + secondNum;
      } else if (newValue[3] === "-") {
        result = firstNum - secondNum;
      }
      var ID = row.id - 1;
      var text = column.text;
      newRow[ID][text] = result;
    }
    this.setState({ rows: newRow });
  }
  render() {
    console.log(this.state);
    const cellEdit = cellEditFactory({
      mode: "click", // click cell to edit
      beforeSaveCell: this.beforeSaveCell
    });
    return (
      <div className="container" style={{ marginTop: 50 }}>
        <BootstrapTable
          striped
          hover
          keyField="id"
          data={this.state.rows}
          columns={this.state.columns}
          cellEdit={cellEdit}
          bootstrap4={true}
        />
        <Button
          variant="primary"
          onClick={this.onAddRow}
          style={{ marginRight: "50px" }}
        >
          Add Row
        </Button>
        <Button variant="primary" onClick={this.onAddColumn}>
          Add Column
        </Button>
      </div>
    );
  }
}

export default App;
